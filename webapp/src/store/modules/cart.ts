import { IProductPayload } from "@/model/Product";
import Vue from "vue";
import VueCookies from "vue-cookies";

Vue.use(VueCookies);

export default {
  namespaced: true,
  state: {
    cart: [] as IProductPayload[],
  },
  mutations: {
    addToCart(state: { cart: IProductPayload[] }, product: IProductPayload) {
      const existingProduct = state.cart.find((item) => item.id === product.id);
      if (existingProduct) {
        existingProduct.quantity =
          (existingProduct.quantity || 0) + (product.quantity || 0);
      } else {
        state.cart.push({ ...product, quantity: product.quantity || 0 });
      }
    },
    removeAllFromCart(state: { cart: IProductPayload[] }) {
      state.cart = [];
    },
    setCart(state: { cart: IProductPayload[] }, product: IProductPayload[]) {
      state.cart = product;
    },
    incrementQuantity(state: { cart: IProductPayload[] }, productId: number) {
      const product = state.cart.find((item) => item.id === productId);
      if (product && product.quantity !== undefined) {
        product.quantity++;
      }
    },
    decreaseQuantity(state: { cart: IProductPayload[] }, productId: number) {
      const productIndex = state.cart.findIndex(
        (item) => item.id === productId
      );
      if (productIndex !== -1) {
        const product = state.cart[productIndex];
        if (product && product.quantity !== undefined) {
          if (product.quantity > 0) {
            product.quantity--;
          }
          if (product.quantity === 0) {
            state.cart.splice(productIndex, 1);
          }
        }
      }
    },
  },
  actions: {
    addToCart(
      context: { commit: (arg0: string, arg1: IProductPayload) => void },
      product: IProductPayload
    ) {
      context.commit("addToCart", product);
    },
    removeAllFromCart(context: { commit: (arg0: string) => void }) {
      context.commit("removeAllFromCart");
    },
    setCart(
      context: { commit: (arg0: string, arg1: IProductPayload[]) => void },
      product: IProductPayload[]
    ) {
      context.commit("setCart", product);
    },
    incrementQuantity(
      context: { commit: (arg0: string, arg1: number) => void },
      productId: number
    ) {
      context.commit("incrementQuantity", productId);
    },
    decreaseQuantity(
      context: { commit: (arg0: string, arg1: number) => void },
      productId: number
    ) {
      context.commit("decreaseQuantity", productId);
    },
  },
  getters: {
    getCart(state: { cart: IProductPayload[] }) {
      return state.cart;
    },
  },
};
