import { IProductPayload } from "./Product";

export interface Cart {
    product: IProductPayload
    quantity: number
}