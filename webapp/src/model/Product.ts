export interface IProduct {
  products: IProductPayload[];
}

export interface IProductPayload {
  id: number;
  title: string;
  description: string;
  photos: string[];
  price: number;
  tags: string[];
  quantity?: number;
}
