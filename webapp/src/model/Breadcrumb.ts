export interface IBreadcrumb {
  title: string;
  disabled: boolean;
  href: string;
}
