import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

export async function getProductApi() {
  const config: AxiosRequestConfig = {
    method: "get",
    url: "http://localhost:3000/",
  };

  const response: AxiosResponse = await axios(config);
  return response;
}
